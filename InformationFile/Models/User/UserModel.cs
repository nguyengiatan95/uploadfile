﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Models.User
{
    public class UserModel
    {
        public int UserID { get; set; }
        public string txtSearch { get; set; }
        public string Email { get; set; }
        public string Fullname { get; set; }
        public int ResourceID { get; set; }
        public int Type { get; set; }
        public int CurrentPage { get; set; }
        public List<UserItem> ListUser { get; set; }
        public List<tblResource> ListResource { get; set; }
        public List<SelectListItem> LstPosition { get; set; }

    }
}