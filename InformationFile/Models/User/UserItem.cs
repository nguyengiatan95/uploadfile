﻿
using InformationFile.Extention;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.User
{
    public class UserItem
    {
        public int UserID { get; set; }
        public string Email { get; set; }
        public string Fullname { get; set; }
        public int ResourceID { get; set; }
        public int Type { get; set; }
        public string ResourceName { get; set; }
        public int CurrentPage { get; set; }
        public string TypeView
        {
            get
            {
                string str = "";
                if (!string.IsNullOrEmpty(Type.ToString()))
                {
                    str = EnumHelper.GetDescription((TypeUserEnum)Type);
                }
                return str;
            }
        }

    }
}