﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.User
{
    public class tbl_User
    {
        public int UserID { get; set; }
        public string Email { get; set; }
        public string Fullname { get; set; }
        public int? ResourceID { get; set; }
        public string ResourceName { get; set; }

        public bool? CmdInsert { get; set; }
        public bool? CmdDelete { get; set; }
        public bool? CmdView { get; set; }
        public bool? CmdEdit { get; set; }
        public int Type { get; set; }
        public int CheckStatus { get; set; }

    }
}