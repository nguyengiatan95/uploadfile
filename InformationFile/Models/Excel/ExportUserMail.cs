﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.Excel
{
    public class ExportUserMail
    {
        public int Stt { get; set; }
        public string HoVaTen { get; set; }
        public string PhongBan { get; set; }
        public string Email { get; set; }
        public string ChucVu { get; set; }
        public string URL { get; set; }
    }
}