﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.Resource
{
    public class ResourceItem
    {
        public int ResourceID { get; set; }
        public string ResourceUrl { get; set; }
        public string Description { get; set; }
        public int? Number { get; set; }
    }
}