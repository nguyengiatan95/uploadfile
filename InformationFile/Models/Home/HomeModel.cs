﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.Home
{
    public class HomeModel
    {
        public HomeModel()
        {
            ListItems = new List<tblResource>();
        }
        public int Id { get; set; }
        public int Department { get; set; }
        public int Staust { get; set; }
        public List<tblResource> ListItems { get; set; }
    }
}