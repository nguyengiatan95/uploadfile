﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.UploadFile
{
    public class TotalFileUploadItem
    {
        public int ResourceID { get; set; }
        public string Description { get; set; }
        public int Number { get; set; }
        public int HieuLucToanPhan { get; set; }
        public int HieuLucTungPhan { get; set; }
        public int HetHieuLuc { get; set; }
        public int Tong { get; set; }
        public string ResourceName { get; set; }
        public bool? checkView { get; set; }
        public int Type { get; set; }
    }
}