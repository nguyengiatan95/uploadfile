﻿
using InformationFile.Extention;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Models.UploadFile
{
    public class UploadFileModel
    {
        public UploadFileModel()
        {
            ListItem = new List<UploadFileModel>();
        }
        public string MediaName { get; set; }
        public int ResourceID { get; set; }
        public int MediaID { get; set; }
        public string MediaUrl { get; set; }
        public int? UserID { get; set; }
        public string MediaType { get; set; }
        public DateTime? Createdate { get; set; }
        public string DeparmentName { get; set; }
        public string FullName { get; set; }
        public List<UploadFileModel> ListItem { get; set; }
        public List<SelectListItem> ListeffectiveStatus { get; set; }

        public string ResourceUrl { get; set; }
        public int CmdInsert { get; set; }
        public int CmdDelete { get; set; }
        public int CmdView { get; set; }

        public string Str { get; set; } = "";
        public int CurrentPage { get; set; }
        public string Url { get; set; }

        public string MediaCode { get; set; }
        public string EffectiveDate { get; set; }
        public int? EffectiveStatus { get; set; }

        public int? EffectiveStatusSearch { get; set; }
        public DateTime? EffectiveDateView { get; set; }

        public DateTime EffectiveDateEdit { get; set; }
        public string EffectiveDateViewEdit
        {
            get
            {
                if (EffectiveDateEdit != DateTime.MinValue)
                {
                    return EffectiveDateEdit.ToString("dd/MM/yyyy");
                }
                return string.Empty;
                
            }
        }

        public string EffectiveStatusView
        {
            get
            {
                string str ="";
                if (!string.IsNullOrEmpty(EffectiveStatus.ToString()))
                {
                    str = EnumHelper.GetDescription((EffectiveStatusEnum)EffectiveStatus);
                }
                return str;
            }
        }
    }
}