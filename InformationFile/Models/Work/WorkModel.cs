﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.Work
{
    public class WorkModel
    {
        public WorkModel()
        {
            ListItem = new List<WorkItemModel>();
        }
        public int WorkId { get; set; }
        public string WorkName { get; set; }
        public string Description { get; set; }
        public string MediaUrl { get; set; }
        public int ResourceID { get; set; }
        public int Status { get; set; }
        public int ToUser { get; set; }
        public List<tblResource> ListResource { get; set; }
        public List<tblUser> ListUser { get; set; }
        public List<WorkItemModel> ListItem { get; set; }
        public int CurrentPage { get; set; }
        public string NoteByLeader { get; set; }
        public string NoteByUser { get; set; }
    }
}