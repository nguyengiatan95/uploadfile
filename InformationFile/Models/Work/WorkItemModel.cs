﻿
using InformationFile.Extention;
using InformationFile.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.Work
{
    public class WorkItemModel
    {
        public int WorkId { get; set; }
        public string WorkName { get; set; }
        public string Description { get; set; }
        public string MediaUrl { get; set; }
        public int ResourceID { get; set; }
        public string ResourceName { get; set; }
        public string UserName { get; set; }
        public int Status { get; set; }
        public DateTime? Createdate { get; set; }
        public string FromUser { get; set; }
        public string ToUser { get; set; }
        public string NoteByLeader { get; set; }
        public string NoteByUser { get; set; }

        public List<tblUser> lstUser { get; set; }
        public List<tbl_User> lstUserWork { get; set; }

        public string StatusView
        {
            get
            {
                return EnumHelper.GetDescription((StatusEnum)Status);
            }
        }


    }
}