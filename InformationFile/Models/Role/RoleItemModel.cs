﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.Role
{
    public class RoleItemModel
    {
        public int? RoleID { get; set; }
        public int? ResourceID { get; set; }
        public string ResourceName { get; set; }
        public int? UserID { get; set; }
        public string UserName { get; set; }
        public Nullable<bool> CmdInsert { get; set; }
        public Nullable<bool> CmdDelete { get; set; }
        public Nullable<bool> CmdView { get; set; }
    }
}