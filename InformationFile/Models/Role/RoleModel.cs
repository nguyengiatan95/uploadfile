﻿using InformationFile.Models.UploadFile;
using InformationFile.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InformationFile.Models.Role
{
    public class RoleModel
    {
        public RoleModel()
        {
            //ListItem = new List<tblResource>();
            ListItem = new List<tbl_User>();
        }
        public List<tbl_User> ListItem { get; set; }
        public string txtSearch { get; set; }
        public int ResourceID { get; set; }
        public int RoleID { get; set; }
        public string UserName { get; set; }
        public int UserID { get; set; }
        public string ResourceName { get; set; }
        public int CurrentPage { get; set; }
        public List<tblUser> ListUser { get; set; }
        public List<tblResource> ListResource { get; set; }
    }
}