﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASPSnippets.GoogleAPI;
using System.Web.Script.Serialization;
using System.Net;
using static InformationFile.Models.Login.LoginModel;
using InformationFile.Models.User;
using InformationFile.Extention;

namespace InformationFile.Controllers
{
    public class LoginController : Controller
    {
        private TimaFMEntities db = new TimaFMEntities();
        // GET: Login
        public ActionResult Index()
        {
            //Session["ErrorLogin"] = "";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void LoginWithGooglePlus()
        {
            // server
            GoogleConnect.ClientId = "899727835740-jo7pit7sr3p2cl0rpunjflupncj2ojl0.apps.googleusercontent.com";
            GoogleConnect.ClientSecret = "4uJ6j5FQhoANEG0Y-kTJXa4Z";

            //localhost
            //GoogleConnect.ClientId = "899727835740-r29183cc98bftiq0b2feukv16eagltpe.apps.googleusercontent.com";
            //GoogleConnect.ClientSecret = "-6qAGpFtT0DEsDlu5hD9coa9";

            GoogleConnect.RedirectUri = Request.Url.AbsoluteUri.Split('?')[0];
            GoogleConnect.Authorize("profile", "email");
            LoginWithGooglePlusConfirmed();
        }
        [ActionName("LoginWithGooglePlus")]
        public ActionResult LoginWithGooglePlusConfirmed()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["code"]))
            {
                string code = Request.QueryString["code"];
                string json = GoogleConnect.Fetch("me", code);
                GoogleProfile profile = new JavaScriptSerializer().Deserialize<GoogleProfile>(json);
                var checkemail = profile.Emails.Find(email => email.Type == "ACCOUNT").Value;
                //checkemail = "hongtx@tima.vn";
                string splitmail = checkemail.Substring(checkemail.Length - 8, 8);
                if (splitmail != "@tima.vn")
                {
                    TempData["Error"] = "Bạn cần nhập mail Tima.!";
                    return RedirectToAction("Index", "Login");
                }
                IQueryable<tblUser> user = db.tblUsers;
                var tblUser = user.FirstOrDefault(m => m.Email == checkemail.Trim() && m.Status == 1);
                //mail tima ko co trong Db
                if (tblUser == null)
                {
                    tbl_User tbluser = new tbl_User();
                    tbluser.Email = checkemail;
                    tbluser.UserID = 0;
                    tbluser.Email = checkemail;
                    tbluser.ResourceID = 0;
                    tbluser.ResourceName = "";
                    tbluser.Fullname = "";
                    tbluser.Type = 0;
                    Session[CommonConstants.USER_LOGIN] = tbluser;
                    Session["UserName"] = checkemail;

                    return RedirectToAction("Index", "TotalFileUpload");
                }
                else
                {
                    tbl_User tbluser = new tbl_User();
                    tbluser.Email = tblUser.Email;
                    tbluser.UserID = tblUser.UserID;
                    tbluser.Email = tblUser.Email;
                    tbluser.ResourceID = tblUser.ResourceID;
                    tbluser.Type = (int)tblUser.Type;
                    tbluser.Fullname = tblUser.Fullname;
                    Session[CommonConstants.USER_LOGIN] = tbluser;
                    Session["UserName"] = tbluser.Email;

                    return RedirectToAction("Index", "TotalFileUpload");
                }

            }
            if (Request.QueryString["error"] == "access_denied")
            {
                return RedirectToAction("Index", "Login");
            }
            return RedirectToAction("Index", "Login");

        }
        [CheckLogin]
        [HttpPost]
        public ActionResult LoadMenu()
        {
            IQueryable<tblRole> tblrole = db.tblRoles;
            IQueryable<tblResource> tblresource = db.tblResources;
            var userid = AuthenUser.CurrentUser().UserID;
            var ResourceID = AuthenUser.CurrentUser().ResourceID;
            //mail ko cos trong db
            if (userid == 0)
            {

                var tblres = tblresource.Where(m => m.Type == (int)TypeResource.TypeAll).ToList();
                var query = tblres.Select(n => new Resoucre()
                {
                    ResourceID = n.ResourceID,
                    ResourceName = n.Description
                }).ToList();
                return Json(query, JsonRequestBehavior.AllowGet);
            }
            //mail boss
            else if (ResourceID == 0)
            {
                var lst = tblresource.OrderBy(m => m.Number).ToList();
                var query = lst.Select(n => new Resoucre
                {
                    ResourceID = n.ResourceID,
                    ResourceName = n.Description
                }).ToList();
                return Json(query, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //var query = (from rl in db.tblRoles
                //             join rs in db.tblResources on rl.ResourceID equals rs.ResourceID
                //             where rl.UserID == userid && rl.CmdView == true
                //             select new { ResourceID = rs.ResourceID, ResourceName = rs.Description, Number = rs.Number }).ToList();

                //query = query.OrderBy(m => m.Number).ToList();

                var role = db.tblRoles.Where(m => m.UserID == userid && m.CmdView == true).ToList();
                var Resource = db.tblResources.FirstOrDefault(m=>m.Type == (int)TypeResource.TypeAll);
                List<tblResource> lstResource = new List<tblResource>();
                lstResource.Add(Resource);
                foreach (var item in role)
                {
                    var Obj = db.tblResources.FirstOrDefault(m => m.ResourceID == item.ResourceID);
                    if (Obj != null)
                    {
                        lstResource.Add(Obj);
                    }
                }

                var query = lstResource.Select(n => new Resoucre
                {
                    ResourceID = n.ResourceID,
                    ResourceName = n.Description,
                    Number = (int)n.Number
                }).ToList();
                query = query.OrderBy(k => k.Number).ToList();
                return Json(query, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Test()
        {

            return View("TestLogin");
        }
        public ActionResult CheckLogin(string mail)
        {
            string splitmail = mail.Substring(mail.Length - 8, 8);
            if (splitmail != "@tima.vn")
            {
                return new JsonResult { Data = new { Status = 0, Message = "Tài khoản không nằm trong hệ thống .!" } };
            }
            IQueryable<tblUser> user = db.tblUsers;
            //mail = "thaont1@tima.vn";
            var tblUser = user.FirstOrDefault(m => m.Email == mail && m.Status == 1);
            //mail tima ko co trong Db
            if (tblUser == null)
            {
                tbl_User tbluser = new tbl_User();
                tbluser.Email = mail;
                tbluser.UserID = 0;
                tbluser.Email = mail;
                tbluser.ResourceID = 0;
                tbluser.ResourceName = "";
                tbluser.Fullname = "";
                tbluser.Type = 0;
                Session[CommonConstants.USER_LOGIN] = tbluser;
                Session["UserName"] = mail;
                //if (tbluser.Type == true)
                //{
                //    Session["Type"] = "1";
                //}
                //else
                //{
                //    Session["Type"] = "0";
                //}
                return new JsonResult { Data = new { Status = 1, Message = "" } };
            }
            else
            {
                tbl_User tbluser = new tbl_User();
                tbluser.Email = tblUser.Email;
                tbluser.UserID = tblUser.UserID;
                tbluser.Email = tblUser.Email;
                tbluser.ResourceID = tblUser.ResourceID;
                //tbluser.ResourceName = db.tblResources.FirstOrDefault(m => m.ResourceID == tblUser.ResourceID).Description; 
                tbluser.Type = (int)tblUser.Type;
                tbluser.Fullname = tblUser.Fullname;
                Session[CommonConstants.USER_LOGIN] = tbluser;
                Session["UserName"] = tbluser.Email;
                //if (tblUser.Type == true)
                //{
                //    Session["Type"] = "1";
                //}
                //else
                //{
                //    Session["Type"] = "0";
                //}
                return new JsonResult { Data = new { Status = 1, Message = "" } };
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return new JsonResult { Data = new { Status = 1, Message = "Done" } };


        }

        public class Resoucre
        {
            public int ResourceID { get; set; }
            public string ResourceName { get; set; }
            public int Number { get; set; }
        }
    }
}