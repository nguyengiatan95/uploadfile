﻿using InformationFile.Extention;
using InformationFile.Models.Role;
using InformationFile.Models.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Controllers
{
    public class RoleController : Controller
    {
        private TimaFMEntities db = new TimaFMEntities();
        // GET: Role
        [CheckLogin]
        public ActionResult Index(RoleModel model)
        {

            //RoleModel model = new RoleModel();
            IQueryable<tblResource> resource = db.tblResources;
            var lstResource = db.tblResources.OrderBy(m=>m.Number).ToList();
            lstResource.Add(new tblResource() { ResourceID = 0, Description = "--Tất cả--" });
            //model.ResourceID = 1;
            ViewBag.UserName = AuthenUser.CurrentUser().Fullname;
            var lstUser = db.tblUsers.ToList();
            if (model.txtSearch != null)
            {
                lstUser = lstUser.Where(m => m.Email.ToLower().Contains(model.txtSearch.ToLower()) || m.Fullname.ToLower().Contains(model.txtSearch.ToLower())).ToList();
            }

            if (model.ResourceID == 0)
            {

                List<tbl_User> lstuser = new List<tbl_User>();

                foreach (var item in lstUser)
                {
                    tbl_User user = new tbl_User();
                    user.UserID = item.UserID;
                    user.Fullname = item.Fullname;
                    user.Email = item.Email;
                    user.ResourceID = 0;
                    lstuser.Add(user);
                }
                model.ListItem = lstuser;

            }
            else
            {

                var query = (from rl in db.tblRoles
                             join us in db.tblUsers on rl.UserID equals us.UserID
                             where rl.ResourceID == model.ResourceID
                             select new { ResourceID = rl.ResourceID, UserID = us.UserID, Fullname = us.Fullname, Email = us.Email, CmdView = rl.CmdView, CmdInsert = rl.CmdInsert, CmdDelete = rl.CmdDelete , CmdEdit = rl.CmdEdit }).ToList();
                List<tbl_User> lstuser = new List<tbl_User>();

                foreach (var i in lstUser)
                {
                    //if (query.Any(p => p.UserID.Contains(i.UserID)))
                    if (query.Any(p => p.UserID == i.UserID))
                    {
                        var role = query.Where(p => p.UserID == i.UserID).FirstOrDefault();
                        var item = new tbl_User()
                        {
                            UserID = role.UserID,
                            Email = role.Email,
                            Fullname = i.Fullname,
                            ResourceID = model.ResourceID,
                            CmdView = role.CmdView,
                            CmdDelete = role.CmdDelete,
                            CmdInsert = role.CmdInsert,
                            CmdEdit = role.CmdEdit

                        };
                        lstuser.Add(item);
                    }
                    else
                    {
                        var item = new tbl_User()
                        {
                            UserID = i.UserID,
                            Email = i.Email,
                            Fullname = i.Fullname,
                            ResourceID = model.ResourceID,
                            CmdView = false,
                            CmdDelete = false,
                            CmdInsert = false,
                            CmdEdit = false

                        };
                        lstuser.Add(item);
                    }
                }
                model.ListItem = lstuser;
                //end else
            }
            //var a = 2;
            var count = model.ListItem.Count();
            model.ListItem = model.ListItem.Skip((model.CurrentPage - 1) * 10).Take(10).ToList();
            model.ResourceID = model.ResourceID;
            model.ListResource = lstResource;
            db.Dispose();
            ViewBag.CurrentPage = model.CurrentPage;
            ViewBag.TotalPage = (int)Math.Ceiling((decimal)count / 10);
            model.txtSearch = model.txtSearch;
            return View(model);
        }
        [HttpPost]
        public JsonResult GetResource(int resourceID)
        {
            RoleItemModel obj = new RoleItemModel();
            IQueryable<tblRole> role = db.tblRoles;
            var userid = AuthenUser.CurrentUser().UserID;
            var objRole = role.FirstOrDefault(m => m.ResourceID == resourceID);
            obj.ResourceName = db.tblResources.FirstOrDefault(n => n.ResourceID == resourceID).Description;
            obj.RoleID = objRole.RoleID;
            obj.ResourceID = objRole.ResourceID;
            //obj.CmdInsert = objRole.CmdInsert;
            //obj.CmdView = objRole.CmdView;
            //obj.CmdDelete = objRole.CmdDelete;

            //obj.UserID = objRole.UserID;
            db.Dispose();

            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [CheckLogin]
        public ActionResult UpdateRole(int userid, int resourceid, bool role, int type)
        {

            IQueryable<tblRole> tblrole = db.tblRoles;
            var ObjRole = tblrole.FirstOrDefault(m => m.ResourceID == resourceid && m.UserID == userid);
            tblRole obj = new tblRole();

            if (ObjRole != null)
            {
                //update
                // type=1 = view , type=2=insert ,type =3=delete ,type= 4= edit
                if (type == 1)
                {
                    obj = db.tblRoles.Find(ObjRole.RoleID);
                    db.Entry(obj).State = EntityState.Modified;
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdView = role;
                    db.SaveChanges();
                }
                if (type == 2)
                {
                    obj = db.tblRoles.Find(ObjRole.RoleID);
                    db.Entry(obj).State = EntityState.Modified;
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdInsert = role;
                    db.SaveChanges();
                }
                if (type == 3)
                {
                    obj = db.tblRoles.Find(ObjRole.RoleID);
                    db.Entry(obj).State = EntityState.Modified;
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdDelete = role;
                    db.SaveChanges();
                }
                if (type == 4)
                {
                    obj = db.tblRoles.Find(ObjRole.RoleID);
                    db.Entry(obj).State = EntityState.Modified;
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdEdit = role;
                    db.SaveChanges();
                }
                int id = obj.RoleID;
                db.Dispose();
                if (id > 0)
                {
                    return new JsonResult { Data = new { Status = 1, Message = "Update thành công .!" } };
                }
                else
                {
                    return new JsonResult { Data = new { Status = 1, Message = "Lỗi .! Update thất bại" } };
                }
            }
            else
            {
                //insert
                if (type == 1)
                {
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdView = role;
                    db.tblRoles.Add(obj);
                    db.SaveChanges();
                }
                if (type == 2)
                {
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdInsert = role;
                    db.tblRoles.Add(obj);
                    db.SaveChanges();
                }
                if (type == 3)
                {
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdDelete = role;
                    db.tblRoles.Add(obj);
                    db.SaveChanges();
                }
                int id = obj.RoleID;
                db.Dispose();
                if (id > 0)
                {
                    return new JsonResult { Data = new { Status = 1, Message = "Update thành công .!" } };
                }
                else
                {
                    return new JsonResult { Data = new { Status = 1, Message = "Lỗi .! Update thất bại" } };
                }

            }

            return new JsonResult { Data = new { Status = 0, Message = "" } };
        }
        [CheckLogin]
        public ActionResult GetRoleUser(int userID, int resourceId)
        {
            IQueryable<tblRole> role = db.tblRoles;
            var obj = role.FirstOrDefault(m => m.UserID == userID && m.ResourceID == resourceId);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

    }
}