﻿
using InformationFile.Extention;
using InformationFile.Models.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Office.Interop.Excel;
using OfficeOpenXml;

namespace InformationFile.Controllers
{
    public class UserController : Controller
    {
        private TimaFMEntities db = new TimaFMEntities();
        // GET: User
        [CheckLogin]
        public ActionResult Index(UserModel model)
        {
            var position = EnumHelper.GetEnumToList(typeof(TypeUserEnum));
            if (position != null && position.Any())
            {
                model.LstPosition = position;
                //SelectListItem itemFirst = new SelectListItem()
                //{
                //    Text = "-- Tất cả --",
                //    Value = "0"
                //};
                //model.LstPosition.Insert(0, itemFirst);
            }

            var lstResource = db.tblResources.ToList();
            if (lstResource != null && lstResource.Any())
            {
                model.ListResource = lstResource;
                tblResource itemFirst = new tblResource()
                {
                    Description = "BOD",
                    ResourceID = 0
                };
                model.ListResource.Insert(0, itemFirst);
            }
            var lstUser = db.tblUsers.ToList();
            if (model.txtSearch != null)
            {
                lstUser = lstUser.Where(n => n.Email.ToLower().Contains(model.txtSearch.ToLower()) || n.Fullname.ToLower().Contains(model.txtSearch.ToLower())).ToList();
            }
            var total = lstUser.Count();
            List<UserItem> lstUserItem = new List<UserItem>();
            foreach (var item in lstUser)
            {
                UserItem objUser = new UserItem();
                if (item.ResourceID == 0)
                {
                    objUser.Email = item.Email;
                    objUser.UserID = item.UserID;
                    objUser.Fullname = item.Fullname;
                    objUser.ResourceID = item.ResourceID;
                    objUser.Type = (int)item.Type;
                    objUser.ResourceName = "BOD";
                }
                else
                {
                    objUser.Email = item.Email;
                    objUser.UserID = item.UserID;
                    objUser.Fullname = item.Fullname;
                    objUser.ResourceID = item.ResourceID;
                    objUser.Type = (int)item.Type;
                    if (db.tblResources.FirstOrDefault(n => n.ResourceID == item.ResourceID) == null)
                    {
                        objUser.ResourceName = "";
                    }
                    else
                    {
                        objUser.ResourceName = db.tblResources.FirstOrDefault(n => n.ResourceID == item.ResourceID).Description;
                    }
                }
                lstUserItem.Add(objUser);
            }
            lstUserItem = lstUserItem.ToList().Skip((model.CurrentPage - 1) * 10).Take(10).ToList();

            model.ListUser = lstUserItem;

            ViewBag.CurrentPage = model.CurrentPage;
            ViewBag.TotalPage = (int)Math.Ceiling((decimal)total / 10);

            return View(model);
        }

        [CheckLogin]
        public ActionResult AddUser(string Email, string FullName, int ResourceID, int? Type)
        {

            if (Email != null)
            {
                var LstMail = db.tblUsers.FirstOrDefault(m => m.Email.ToLower() == Email);
                if (LstMail != null)
                {
                    return new JsonResult { Data = new { Status = 0, Message = "Mail này đã tồn tại.!" } };
                }
                //thêm email
                tblUser user = new tblUser();
                user.Email = Email;
                user.Fullname = FullName;
                user.ResourceID = ResourceID;
                user.Type = Type;
                user.Status = 1;

                db.tblUsers.Add(user);
                db.SaveChanges();

                int id = user.UserID;
                //BOD có quyền xem tất cả các phòng ban
                if (ResourceID == 0 || Type == TypeUserEnum.BOD.GetHashCode() || Type == TypeUserEnum.PhanQuyen.GetHashCode())
                {
                    var lstResource = db.tblResources.ToList();
                    foreach (var item in lstResource)
                    {
                        tblRole obj = new tblRole();
                        obj.UserID = id;
                        obj.ResourceID = item.ResourceID;
                        if (Type == TypeUserEnum.PhanQuyen.GetHashCode())
                        {
                            obj.CmdInsert = true;
                            obj.CmdEdit = true;
                            obj.CmdDelete = true;
                        }
                        obj.CmdView = true;
                        db.tblRoles.Add(obj);
                        db.SaveChanges();

                        int insert = obj.RoleID;
                        if (insert > 0)
                        {
                        }
                        else
                        {
                            db.Dispose();
                            return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Thêm mới quyền BOD" + ResourceID + "" } };
                        }
                    }
                    return new JsonResult { Data = new { Status = 1, Message = "Tạo mới  thành công .!" } };
                }
                //Quyền xem  của phòng
                if (ResourceID != 0)
                {
                    tblRole obj = new tblRole();
                    obj.UserID = id;
                    if (Type == TypeUserEnum.TruongPhong.GetHashCode())
                    {
                        obj.CmdInsert = true;
                        obj.CmdEdit = true;
                        obj.CmdDelete = true;
                    }
                    obj.ResourceID = ResourceID;
                    obj.CmdView = true;
                    db.tblRoles.Add(obj);
                    db.SaveChanges();
                    db.Dispose();
                    int insert = obj.RoleID;
                    if (insert > 0)
                    {
                    }
                    else
                    {
                        db.Dispose();
                        return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Thêm mới quyền trưởng phòng" + ResourceID + "" } };
                    }
                }
                if (id > 0)
                {
                    db.Dispose();
                    return new JsonResult { Data = new { Status = 1, Message = "Tạo mới  thành công .!" } };
                }
                else
                {
                    return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! tạo mới  thất bại" } };
                }
            }
            return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! tạo mới  thất bại" } };
        }

        [CheckLogin]
        public ActionResult DeleteUser(int UserID)
        {
            var objResource = db.tblUsers.FirstOrDefault(m => m.UserID == UserID);
            if (objResource != null)
            {
                tblUser user = db.tblUsers.Find(UserID);
                db.tblUsers.Remove(user);
                db.SaveChanges();
                db.Dispose();
                return new JsonResult { Data = new { Status = 1, Message = "Xóa gmail thành công" } };
            }
            else
            {
                return new JsonResult { Data = new { Status = 0, Message = "Xóa gmail thất bại" } };
            }

        }

        [CheckLogin]
        public JsonResult GetUserById(int UserID)
        {
            tblUser user = new tblUser();
            var obj = db.tblUsers.FirstOrDefault(m => m.UserID == UserID);
            if (obj != null)
            {
                user.UserID = obj.UserID;
                user.Fullname = obj.Fullname;
                user.Email = obj.Email;
                user.ResourceID = obj.ResourceID;
                user.Type = obj.Type;
            }
            db.Dispose();
            return Json(user, JsonRequestBehavior.AllowGet);
        }
        [CheckLogin]
        public ActionResult UpdateUser(int UserID, string Email, string Fullname, int Type, int ResourceID = 0)
        {
            var ObjUser = db.tblUsers.FirstOrDefault(m => m.UserID == UserID);
            if (ObjUser != null)
            {
                tblUser obj = new tblUser();
                obj = db.tblUsers.Find(UserID);
                db.Entry(obj).State = EntityState.Modified;
                obj.Fullname = Fullname;
                obj.Email = Email;
                obj.Type = Type;
                obj.ResourceID = ResourceID;
                db.SaveChanges();
                //ckeck lại quyền
                var lstRole = db.tblRoles.Where(m => m.UserID == UserID);
                if (lstRole != null)
                {
                    foreach (var item in lstRole)
                    {
                        tblRole role = db.tblRoles.Find(item.RoleID);
                        db.tblRoles.Remove(role);
                    }
                    db.SaveChanges();
                }
                PermissionRole(UserID, ResourceID, Type);
                db.Dispose();
                return new JsonResult { Data = new { Status = 1, Message = "Cập nhật user thành công" } };
            }
            return new JsonResult { Data = new { Status = 0, Message = "Lỗi.Không tồn tại user" } };
        }
        public ActionResult PermissionRole(int UserId, int ResourceID, int Type)
        {
            //BOD có quyền xem tất cả các phòng ban
            if (ResourceID == 0 || Type == TypeUserEnum.BOD.GetHashCode() || Type == TypeUserEnum.PhanQuyen.GetHashCode())
            {
                var lstResource = db.tblResources.ToList();
                foreach (var item in lstResource)
                {
                    tblRole obj = new tblRole();
                    obj.UserID = UserId;
                    obj.ResourceID = item.ResourceID;
                    if (Type == TypeUserEnum.PhanQuyen.GetHashCode())
                    {
                        obj.CmdInsert = true;
                        obj.CmdEdit = true;
                        obj.CmdDelete = true;
                    }
                    obj.CmdView = true;
                    db.tblRoles.Add(obj);
                    db.SaveChanges();

                    int insert = obj.RoleID;
                    if (insert > 0)
                    {
                    }
                    else
                    {
                        db.Dispose();
                        return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Thêm mới quyền BOD" + ResourceID + "" } };
                    }
                }
                return new JsonResult { Data = new { Status = 1, Message = "Tạo mới  thành công .!" } };
            }
            //Quyền xem  của phòng
            if (ResourceID != 0)
            {
                tblRole obj = new tblRole();
                obj.UserID = UserId;
                obj.ResourceID = ResourceID;
                if (Type == TypeUserEnum.TruongPhong.GetHashCode())
                {
                    obj.CmdInsert = true;
                    obj.CmdEdit = true;
                    obj.CmdDelete = true;
                }
                obj.CmdView = true;
                db.tblRoles.Add(obj);
                db.SaveChanges();
                db.Dispose();
                int insert = obj.RoleID;
                if (insert > 0)
                {

                }
                else
                {
                    db.Dispose();
                    return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Thêm mới quyền trưởng phòng" + ResourceID + "" } };
                }
            }
            return null;
        }

        public ActionResult UploadExcel()
        {
            string str = string.Empty;
            string fname = "";
            string fileExtnew = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;


                    HttpPostedFileBase file = files[0];
                    //var supportedTypes = new[] { "txt", "doc", "docx", "pdf", "xls", "xlsx" };

                    var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                    string date = DateTime.Now.ToString("dd/MM/yyyy HH:mm").Replace("/", "_").Replace(" ", "_").Replace(":", "_");

                    fname = Path.GetFileName(date + file.FileName);
                    if (!fileExt.Contains("xls") || !fileExt.Contains("xlsx"))
                    {
                        return new JsonResult { Data = new { Status = 0, Message = "File không đúng định dạng" } };
                    }
                    var path = "~/Data";
                    // Get the complete folder path and store the file inside it.  
                    var newpath = Path.Combine(Server.MapPath(path), fname);

                    file.SaveAs(newpath);

                    //Application ap = new Application();
                    //Workbook wb = ap.Workbooks.Open(newpath);
                    //Worksheet ws = wb.ActiveSheet;
                    //Range rg = ws.UsedRange;
                    //List<tblUser> ListUser = new List<tblUser>();
                    //for (int k = 3; k <= rg.Rows.Count; k++)
                    //{
                    //    tblUser user = new tblUser();
                    //    user.Email = ((Range)rg.Cells[k, 4]).Text;
                    //    user.Fullname = ((Range)rg.Cells[k, 2]).Text;
                    //    user.ResourceID = CheckResource(((Range)rg.Cells[k, 6]).Text);
                    //    user.Type = CheckType(((Range)rg.Cells[k, 5]).Text);
                    //    str += AddUserAndPermissionRole(user.Email, user.Fullname, user.ResourceID, user.Type);
                    //}

                    FileInfo existingFile = new FileInfo(newpath);
                    using (ExcelPackage package = new ExcelPackage(existingFile))
                    {
                        //get the first worksheet in the workbook
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                        int colCount = worksheet.Dimension.End.Column;  //get Column Count
                        int rowCount = worksheet.Dimension.End.Row;     //get row count
                        for (int row = 2; row <= rowCount; row++)
                        {
                            tblUser user = new tblUser();
                            user.Email = worksheet.Cells[row, 4].Value.ToString().Trim();
                            user.Fullname = worksheet.Cells[row, 2].Value.ToString().Trim();
                            user.ResourceID = CheckResource(worksheet.Cells[row, 6].Value.ToString().Trim());
                            user.Type = CheckType(worksheet.Cells[row, 5].Value.ToString().Trim());
                            str += AddUserAndPermissionRole(user.Email, user.Fullname, user.ResourceID, user.Type);
                        }
                    }
                    db.Dispose();
                    return new JsonResult { Data = new { Status = 1, Message = str } };
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            return new JsonResult { Data = new { Status = 0, Message = "Bạn chưa chọn file" } };
        }
        public int CheckResource(string resource)
        {
            int id = 0;
            resource = "/" + resource;
            var resourceId = db.tblResources.FirstOrDefault(m => m.ResourceUrl == resource);
            if (resourceId != null)
            {
                id = resourceId.ResourceID;
            }
            return id;
        }
        public int CheckType(string Type)
        {
            int id = 0;
            var LstType = EnumHelper.GetEnumToList(typeof(TypeUserEnum));

            var typeid = LstType.FirstOrDefault(m => m.Text == Type);
            if (typeid != null)
            {
                id = Int32.Parse(typeid.Value.ToString());
            }
            return id;
        }
        public string AddUserAndPermissionRole(string Email, string FullName, int ResourceID, int? Type)
        {
            string str = string.Empty;
            if (Email != null)
            {
                var LstMail = db.tblUsers.FirstOrDefault(m => m.Email.ToLower() == Email);
                if (LstMail != null)
                {
                    str = " Mail" + Email + "này đã tồn tại!";
                    return str;
                }
                //thêm email
                tblUser user = new tblUser();
                user.Email = Email;
                user.Fullname = FullName;
                user.ResourceID = ResourceID;
                user.Type = Type;
                user.Status = 1;

                db.tblUsers.Add(user);
                db.SaveChanges();

                int id = user.UserID;
                //BOD có quyền xem tất cả các phòng ban
                if (ResourceID == 0 || Type == TypeUserEnum.BOD.GetHashCode() || Type == TypeUserEnum.PhanQuyen.GetHashCode())
                {
                    var lstResource = db.tblResources.ToList();
                    foreach (var item in lstResource)
                    {
                        tblRole obj = new tblRole();
                        obj.UserID = id;
                        obj.ResourceID = item.ResourceID;
                        if (Type == TypeUserEnum.PhanQuyen.GetHashCode())
                        {
                            obj.CmdInsert = true;
                            obj.CmdEdit = true;
                            obj.CmdDelete = true;
                        }
                        obj.CmdView = true;
                        db.tblRoles.Add(obj);
                        db.SaveChanges();

                        int insert = obj.RoleID;
                        if (insert > 0)
                        {

                        }
                        else
                        {

                            str = " Thêm mới quyền BOD" + ResourceID + "của mail" + Email;
                            return str;
                        }
                    }
                }
                //Quyền xem  của phòng
                if (ResourceID != 0)
                {
                    var roleuser = db.tblRoles.FirstOrDefault(m => m.UserID == id && m.ResourceID == ResourceID);
                    if (roleuser != null)
                    {
                        return str;
                    }
                    tblRole obj = new tblRole();
                    obj.UserID = id;
                    obj.ResourceID = ResourceID;
                    if (Type == TypeUserEnum.TruongPhong.GetHashCode())
                    {
                        obj.CmdInsert = true;
                        obj.CmdEdit = true;
                        obj.CmdDelete = true;
                    }
                    obj.CmdView = true;
                    db.tblRoles.Add(obj);
                    db.SaveChanges();
                    int insert = obj.RoleID;
                    if (insert < 0)
                    {
                        //return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Thêm mới quyền trưởng phòng" + ResourceID + "" } };
                        str = "Lỗi .! Thêm mới quyền trưởng phòng" + ResourceID + "của mail" + Email;
                        return str;
                    }
                    else
                    {
                        return str;
                    }
                }
                if (id > 0)
                {
                    return str;
                }
                else
                {
                    str = "Lỗi .!" + Email;
                    return str;
                }
            }
            return str;
        }
    }
}