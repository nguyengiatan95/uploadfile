﻿using InformationFile.Extention;
using InformationFile.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Controllers
{
    public class HomeController : Controller
    {
        private TimaFMEntities db = new TimaFMEntities();
        [CheckLogin]
        public ActionResult Index(HomeModel model)
        {
            //IQueryable<tbl_Department> department = db.tbl_Department;
            //var lst = department.Where( m =>m.Id ==1).ToList();
            //model.ListItems = lst;
            return View();
        }
        [HttpPost]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact(int id)
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}