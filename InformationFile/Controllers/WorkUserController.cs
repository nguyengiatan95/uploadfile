﻿using InformationFile.Extention;
using InformationFile.Models.Work;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Controllers
{
    public class WorkUserController : Controller
    {
        private TimaFMEntities db = new TimaFMEntities();
        // GET: WorkUser
        [CheckLogin]
        public ActionResult Index(WorkModel model)
        {
            ViewBag.UserName  = AuthenUser.CurrentUser().Fullname;
            var userid = AuthenUser.CurrentUser().UserID;


            model.CurrentPage = model.CurrentPage == 0 ? 1 : model.CurrentPage;
            //var lstwork = db.tblWorkUsers.Where(m => m.ToUser == userid).ToList();
            //var total = lstwork.Count();

            //lstwork.Skip((model.CurrentPage - 1) * 10).Take(10).ToList();
            //model.ListItem = lstwork.Select(m => new WorkItemModel()
            //{
            //    WorkId = m.WorkId,
            //    Description = db.tblWorks.FirstOrDefault(l=>l.WorkId==m.WorkId).Description,
            //    WorkName = db.tblWorks.FirstOrDefault(l => l.WorkId == m.WorkId).WorkName,
            //    MediaUrl = db.tblWorks.FirstOrDefault(l => l.WorkId == m.WorkId).MediaUrl,
            //    Createdate = db.tblWorks.FirstOrDefault(l => l.WorkId == m.WorkId).Createdate ,
            //    ResourceName = db.tblWorks.FirstOrDefault(l => l.WorkId == m.WorkId).MediaUrl,
            //    FromUser = db.tblUsers.FirstOrDefault(n => n.UserID == m.UserID).Fullname,
            //    Status = (int)m.Status
            //    //Url = "/DATA" + resource.FirstOrDefault(k => k.ResourceID == m.ResourceID).ResourceUrl + "/" + m.MediaUrl

            //}).ToList();

            var lstWorkUser = (from workUser in db.tblWorkUsers
                     join work in db.tblWorks on workUser.WorkId equals work.WorkId
                     join resources in db.tblResources on work.ResourceID equals resources.ResourceID
                     where workUser.ToUser == userid
                     select new {
                         WorkId = workUser.WorkId ,
                         WorkName = work.WorkName,
                         Description = work.Description,
                         MediaUrl = work.MediaUrl,
                         Createdate = work.Createdate,
                         ResourceName = resources.Description,
                         ResourceID = work.ResourceID,
                         UserID = work.UserID,
                         Status = work.Status,
                         NoteByLeader = work.NoteByLeader
                     }).ToList();
            var total = lstWorkUser.Count();
            lstWorkUser.OrderByDescending(k => k.Createdate).Skip((model.CurrentPage - 1) * 10).Take(10).ToList();

            model.ListItem = lstWorkUser.Select(m => new WorkItemModel()
            {
                WorkId = m.WorkId,
                Description = m.Description,
                WorkName = m.WorkName,
                MediaUrl = m.MediaUrl,
                Createdate = m.Createdate,
                ResourceName = m.ResourceName,
                ResourceID = (int)m.ResourceID,
                FromUser = db.tblUsers.FirstOrDefault(l=>l.UserID ==m.UserID).Fullname,
                Status = (int)m.Status,
                NoteByLeader = m.NoteByLeader

                //MediaUrl = "/DATA" + db.tblResources.FirstOrDefault(k => k.ResourceID == m.ResourceID).ResourceUrl + "/" + m.MediaUrl

            }).ToList();
            ViewBag.CurrentPage = model.CurrentPage;
            ViewBag.TotalPage = (int)Math.Ceiling((decimal)total / 10);

            return View(model);
        }

        public ActionResult UpdateStatusWork(int workId, string notebyuser,int status)
        {
            try
            {
                var userid = AuthenUser.CurrentUser().UserID;

                tblWork wk = new tblWork();
                wk = db.tblWorks.Find(workId);
                db.Entry(wk).State = EntityState.Modified;
                wk.NoteByUser = notebyuser;
                if (status ==1)
                {
                    wk.Status = StatusEnum.HoanThanh.GetHashCode();
                    SendMailSuccessWork(workId);
                }
                else
                {
                    wk.Status = StatusEnum.DangXuLy.GetHashCode();
                }
                wk.DoneByUser = userid;
                db.SaveChanges();
                db.Dispose();
                return new JsonResult { Data = new { Status = 1, Message = "Cập nhật công việc thành công" } };
            }
            catch (Exception e)
            {
                ////var a = e.Message;
                return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Cập nhật công việc  thất bại" } };
            }
        }
        [CheckLogin]
        [HttpPost]
        public JsonResult ExporFile(int id )
        {
            string fileName = "";
            if (id > 0)
            {
                IQueryable<tblWork> work = db.tblWorks;
                fileName = work.FirstOrDefault(m => m.WorkId == id).MediaUrl;
                db.Dispose();
                return Json(new { fileName = fileName, errorMessage = "" });
            }
            else
            {
                return Json(new { fileName = fileName, errorMessage = "Không tìm thấy file" });
            }

        }
        [CheckLogin]
        [HttpGet]
        public ActionResult Download(string file, int resourceID)
        {
            //get the temp folder and file path in server
            IQueryable<tblResource> resource = db.tblResources;
            var resourceUrl = resource.FirstOrDefault(k => k.ResourceID == resourceID).ResourceUrl;
            var path = "~/Data" + resourceUrl;
            db.Dispose();

            string fullPath = Path.Combine(Server.MapPath(path), file);

            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(fullPath, "application/vnd.ms-excel", file);
        }

        public void SendMailSuccessWork(int workID)
        {
            var userid = AuthenUser.CurrentUser().UserID;
            var work = db.tblWorks.FirstOrDefault(m => m.WorkId == workID);
            if (work.AssignByUser > 0 )
            {
                var user = db.tblUsers.FirstOrDefault(n => n.UserID == work.AssignByUser);
                var subject = "Hoàn thành công việc" + work.WorkName;
                SendMailTima.SendEmail(user.Email, subject, work.Description);
            }

        }
    }
}