﻿using InformationFile.Extention;
using InformationFile.Models.UploadFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Controllers
{
    public class TotalFileUploadController : Controller
    {
        private TimaFMEntities db = new TimaFMEntities();
        // GET: TotalFileUpload
        [CheckLogin]
        public ActionResult Index(TotalFileUploadModel model)
        {
            var UserId = AuthenUser.CurrentUser().UserID;
            var lstMedia = db.tblMedias.ToList();
            var lstResource = db.tblResources.ToList();
            var typeUser = AuthenUser.CurrentUser().Type;
            if (UserId > 0)
            {
                List<TotalFileUploadItem> lst = new List<TotalFileUploadItem>();
                foreach (var item in lstResource)
                {
                    if (lstMedia.Any(p => p.ResourceID == item.ResourceID))
                    {
                        TotalFileUploadItem obj = new TotalFileUploadItem();
                        obj.ResourceID = item.ResourceID;
                        obj.ResourceName = item.Description;
                        obj.HieuLucToanPhan = lstMedia.Count(m => m.ResourceID == item.ResourceID && m.EffectiveStatus == EffectiveStatusEnum.HieuLucToanPhan.GetHashCode());
                        obj.HieuLucTungPhan = lstMedia.Count(m => m.ResourceID == item.ResourceID && m.EffectiveStatus == EffectiveStatusEnum.HieuLucTungPhan.GetHashCode());
                        obj.HetHieuLuc = lstMedia.Count(m => m.ResourceID == item.ResourceID && m.EffectiveStatus == EffectiveStatusEnum.HetHieuLuc.GetHashCode());
                        obj.Tong = lstMedia.Count(m => m.ResourceID == item.ResourceID);
                        obj.Number = (int)item.Number;
                        obj.Type = item.Type == null ? 0 : (int)item.Type;
                        if (typeUser == TypeUserEnum.BOD.GetHashCode() || typeUser == TypeUserEnum.PhanQuyen.GetHashCode())
                        {
                            obj.checkView = true;
                        }
                        else
                        {
                            var role = db.tblRoles.FirstOrDefault(l => l.ResourceID == item.ResourceID && l.UserID == UserId);
                            if (role != null)
                            {
                                obj.checkView = role.CmdView;
                            }
                        }

                        lst.Add(obj);
                    }
                    else
                    {

                        TotalFileUploadItem obj = new TotalFileUploadItem();
                        obj.ResourceID = item.ResourceID;
                        obj.ResourceName = item.Description;
                        obj.HieuLucToanPhan = 0;
                        obj.HieuLucTungPhan = 0;
                        obj.HetHieuLuc = 0;
                        obj.Tong = 0;
                        obj.Number = (int)item.Number;
                        obj.Type = item.Type == null ? 0 : (int)item.Type;
                        if (typeUser == TypeUserEnum.BOD.GetHashCode() || typeUser == TypeUserEnum.PhanQuyen.GetHashCode())
                        {
                            obj.checkView = true;
                        }
                        else
                        {
                            var role = db.tblRoles.FirstOrDefault(l => l.ResourceID == item.ResourceID && l.UserID == UserId);
                            if (role != null)
                            {
                                obj.checkView = role.CmdView;
                            }
                        }

                        lst.Add(obj);
                    }
                }
                db.Dispose();
                model.lstItem = lst.OrderBy(m => m.Number).ToList();
            }
            else
            {
                List<TotalFileUploadItem> lst = new List<TotalFileUploadItem>();
                var list = db.tblResources.Where(m => m.ResourceID == 4).ToList();
                model.lstItem = list.Select(n => new TotalFileUploadItem()
                {
                    ResourceID = n.ResourceID,
                    ResourceName = n.Description,
                    HieuLucToanPhan = lstMedia.Count(m => m.ResourceID == n.ResourceID && m.EffectiveStatus == EffectiveStatusEnum.HieuLucToanPhan.GetHashCode()),
                    HieuLucTungPhan = lstMedia.Count(m => m.ResourceID == n.ResourceID && m.EffectiveStatus == EffectiveStatusEnum.HieuLucTungPhan.GetHashCode()),
                    HetHieuLuc = lstMedia.Count(m => m.ResourceID == n.ResourceID && m.EffectiveStatus == EffectiveStatusEnum.HetHieuLuc.GetHashCode()),
                    Tong = lstMedia.Count(m => m.ResourceID == n.ResourceID),
                    Type = n.Type == null ? 0 : (int)n.Type,
                checkView = true
                }).ToList();
            }
            return View(model);
        }
    }
}