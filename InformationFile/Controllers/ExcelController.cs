﻿using InformationFile.Models.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using InformationFile.Extention;

namespace InformationFile.Controllers
{
    public class ExcelController : Controller
    {
        // GET: Excel
        private TimaFMEntities db = new TimaFMEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ExportUserMail()
        {


            var position = EnumHelper.GetEnumToList(typeof(TypeUserEnum));
            List<ExportUserMail> lstUserMail = new List<ExportUserMail>();

            var lstData = db.tblResources.ToList();
            int i = 1;
            int j = 0;
            foreach (var item in lstData)
            {
                var obj = new ExportUserMail();
                obj.Stt = i;
                obj.HoVaTen = "Nguyễn Văn A" + i;
                obj.PhongBan = item.Description;
                obj.Email = "Email" + i + "@tima.vn";
                obj.ChucVu = position[j].Text;
                j++;
                if (j == position.Count)
                {
                    j = 0;
                }
                var url = item.ResourceUrl.Substring(1);
                obj.URL = url;
                i++;
                lstUserMail.Add(obj);
            }
            SaveExportUserMail(lstUserMail);
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + string.Format("{0} - File mẫu Upload mail nhân viên.xlsx", DateTime.Today.ToString("yyyyMMdd")));
            return new EmptyResult();
        }

        public void SaveExportUserMail(List<ExportUserMail> lstData)
        {
            List<string> lstHeaderReportTDHS = new List<string> { "STT", "Họ và tên", "Phòng/Ban", "Email", "Chức vụ", "Url" };
            List<int> lstWidthReportTDHS = new List<int> { 10, 20, 40, 40, 50, 50 };
            List<ExcelHorizontalAlignment> lstAlign = new List<ExcelHorizontalAlignment> {
                                                                                           ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Center,
                                                                                           ExcelHorizontalAlignment.Center};
            List<System.Drawing.Color> lstColor = new List<System.Drawing.Color> {
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black,
                                                                                   Color.Black};
            ExcelPackage package = new ExcelPackage();
            var ws = package.Workbook.Worksheets.Add("File mẫu Upload mail nhân viên");
            // Set Font
            ws.Cells.Style.Font.Name = "Calibri";
            ws.Cells.Style.Font.Size = 11;

            // Format All Cells
            ws.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            // Format All Column
            for (int i = 1; i <= lstHeaderReportTDHS.Count; i++)
            {
                ws.Column(i).Width = lstWidthReportTDHS[i - 1];
                ws.Column(i).Style.HorizontalAlignment = lstAlign[i - 1];
                ws.Column(i).Style.Font.Color.SetColor(lstColor[i - 1]);
            }

            int iPosHeader = 1;
            // mapping title header
            ws.Cells[iPosHeader, 2, iPosHeader, lstHeaderReportTDHS.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[iPosHeader, 2, iPosHeader, lstHeaderReportTDHS.Count].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(77, 119, 204));
            ws.Cells[iPosHeader, 2, iPosHeader, lstHeaderReportTDHS.Count].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(255, 255, 255));
            ws.Cells[iPosHeader, 2, iPosHeader, lstHeaderReportTDHS.Count].Style.Font.Bold = true;
            ws.Row(iPosHeader).Height = 16;
            for (int i = 1; i <= lstHeaderReportTDHS.Count; i++)
            {
                ws.Cells[iPosHeader, i].Value = lstHeaderReportTDHS[i - 1];
            }
            // SET VALUE
            iPosHeader++;
            int stt = 0;
            for (int i = 0; i < lstData.Count; i++)
            {
                ws.Cells[iPosHeader, 1].Value = lstData[i].Stt;
                ws.Cells[iPosHeader, 2].Value = lstData[i].HoVaTen;
                ws.Cells[iPosHeader, 3].Value = lstData[i].PhongBan;
                ws.Cells[iPosHeader, 4].Value = lstData[i].Email;
                ws.Cells[iPosHeader, 5].Value = lstData[i].ChucVu;
                ws.Cells[iPosHeader, 6].Value = lstData[i].URL;
                iPosHeader++;
            }
            package.SaveAs(HttpContext.Response.OutputStream);
        }
    }
}