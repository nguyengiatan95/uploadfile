﻿using InformationFile.Extention;
using InformationFile.Models.Resource;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Controllers
{
    public class ResourceController : Controller
    {
        // GET: Resource
        private TimaFMEntities db = new TimaFMEntities();
        [CheckLogin]
        public ActionResult Index()
        {
            ResourceModel model = new ResourceModel();
            var lstResource = db.tblResources.ToList();
            model.ListItem = lstResource.Select(m => new ResourceItem()
            {
                ResourceID = m.ResourceID,
                Description = m.Description,
                ResourceUrl = m.ResourceUrl,
                Number = m.Number
            }).ToList();

            ViewBag.Email = AuthenUser.CurrentUser().Email;
            return View(model);
        }
        [CheckLogin]
        public ActionResult AddResource(string Resource , string ResourceUrl , int Number = 0)
        {
            string Url = "";
            if (Resource != null || ResourceUrl != null)
            {

                Url = "/" + ResourceUrl.Trim();
               string CreateUrl = "~/DATA" + Url;
                if (!System.IO.Directory.Exists(Server.MapPath(CreateUrl)))
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath(CreateUrl));
                }
                tblResource resource = new tblResource();
                resource.Description = Resource;
                resource.ResourceUrl= Url;
                resource.Number = Number;

                db.tblResources.Add(resource);
                db.SaveChanges();
                db.Dispose();
                int id = resource.ResourceID;
                if (id > 0)
                {
                    return new JsonResult { Data = new { Status = 1, Message = "Tạo mới  thành công .!" } };
                }
                else
                {
                    return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! tạo mới  thất bại" } };
                }
            }
            return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! tạo mới  thất bại" } };
        }
        [CheckLogin]
        public JsonResult GetResourceById(int resourceID)
        {
            ResourceItem resource = new ResourceItem();
            var obj = db.tblResources.FirstOrDefault(m => m.ResourceID == resourceID);
            if (obj != null)
            {
                resource.ResourceID = obj.ResourceID;
                resource.Number = obj.Number;
                resource.Description = obj.Description;
            }
            db.Dispose();
            return Json(resource, JsonRequestBehavior.AllowGet);
        }
        [CheckLogin]
        public ActionResult UpdateResource(int ResourceID, string Description, int Number = 0)
        {
            var ObjResource = db.tblResources.FirstOrDefault(m => m.ResourceID == ResourceID);
            if (ObjResource != null)
            {
                tblResource obj = new tblResource();
                obj = db.tblResources.Find(ResourceID);
                db.Entry(obj).State = EntityState.Modified;
                obj.Description = Description;
                obj.Number = Number;
                db.SaveChanges();
                db.Dispose();
                return new JsonResult { Data = new { Status = 1, Message = "Cập nhật phòng ban thành công" } };
            }
            return new JsonResult { Data = new { Status = 0, Message = "Lỗi.Không tồn tại phòng ban" } };
        }
        [CheckLogin]
        public ActionResult DeleteResource(int ResourceID)
        {
            var objResource = db.tblResources.FirstOrDefault(m => m.ResourceID == ResourceID);
            if (objResource != null)
            {
                tblResource resource = db.tblResources.Find(ResourceID);
                db.tblResources.Remove(resource);
                db.SaveChanges();
                db.Dispose();
                return new JsonResult { Data = new { Status = 1, Message = "Xóa phòng ban thành công" } };
            }
            else
            {
                return new JsonResult { Data = new { Status = 0, Message = "Xóa phòng ban thất bại" } };
            }

        }
    }
}