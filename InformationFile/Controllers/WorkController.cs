﻿using InformationFile.Extention;
using InformationFile.Models.User;
using InformationFile.Models.Work;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Controllers
{
    public class WorkController : Controller
    {
        private TimaFMEntities db = new TimaFMEntities();
        // GET: Work
        [CheckLogin]
        public ActionResult Index(WorkModel model)
        {
            model.CurrentPage = model.CurrentPage == 0 ? 1 : model.CurrentPage;
            ViewBag.UserName = AuthenUser.CurrentUser().Fullname;

            var lstResource = db.tblResources.ToList();
            lstResource.Add(new tblResource() { ResourceID = 0, Description = "--Tất cả--" });
            model.ListResource = lstResource;
            var userid = AuthenUser.CurrentUser().UserID;

            var lstwork = db.tblWorks.Where(m => m.UserID == userid).ToList();
            var total = lstwork.Count();
            model.ListItem = lstwork.Select(m => new WorkItemModel()
            {
                WorkId = m.WorkId,
                Description = m.Description,
                WorkName = m.WorkName,
                MediaUrl = m.MediaUrl,
                Createdate = m.Createdate,
                ResourceName = db.tblResources.FirstOrDefault(n => n.ResourceID == m.ResourceID).Description,
                FromUser = db.tblUsers.FirstOrDefault(n => n.UserID == m.UserID).Fullname,
                Status = (int)m.Status
                //Url = "/DATA" + resource.FirstOrDefault(k => k.ResourceID == m.ResourceID).ResourceUrl + "/" + m.MediaUrl

            }).ToList().OrderByDescending(k => k.Createdate).Skip((model.CurrentPage - 1) * 10).Take(10).ToList();

            //model.ListItem.OrderByDescending(l =>l.Createdate).ToList();
            model.ListItem = model.ListItem;

            ViewBag.CurrentPage = model.CurrentPage;
            ViewBag.TotalPage = (int)Math.Ceiling((decimal)total / 10);

            ViewBag.ResourceName = AuthenUser.CurrentUser().ResourceName;
            return View(model);
        }
        [CheckLogin]
        [HttpPost]
        public ActionResult AddWork(string workName, string description, int resourceID)
        {
            string fname = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];

                        var supportedTypes = new[] { "txt", "doc", "docx", "pdf", "xls", "xlsx" };
                        var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                        //Checking for Internet Explorer
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string date = DateTime.Now.ToString("dd/MM/yyyy HH:mm").Replace("/", "_").Replace(" ", "_").Replace(":", "_");

                            fname = Path.GetFileName(date + file.FileName);
                            if (fileExt.Contains("exe"))
                            {
                                return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Upload file thất bại" } };
                            }
                        }
                        IQueryable<tblResource> resource = db.tblResources;
                        var resourceUrl = resource.FirstOrDefault(k => k.ResourceID == resourceID).ResourceUrl;
                        var path = "~/Data" + resourceUrl;
                        // Get the complete folder path and store the file inside it.  
                        var newpath = Path.Combine(Server.MapPath(path), fname);
                        file.SaveAs(newpath);
                    }
                    // Returns message that successfully uploaded  

                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            tblWork work = new tblWork();
            work.WorkName = workName;
            work.Description = description;
            work.MediaUrl = fname;
            work.Createdate = DateTime.Now;
            //work.MediaType = fileExt;
            work.ResourceID = resourceID;
            work.UserID = AuthenUser.CurrentUser().UserID;
            work.Status = (int)StatusEnum.DaGui;
            db.tblWorks.Add(work);
            db.SaveChanges();
            db.Dispose();
            int id = work.WorkId;
            if (id > 0)
            {
                return new JsonResult { Data = new { Status = 1, Message = "Tạo mới  thành công .!" } };
            }
            else
            {
                return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! tạo mới  thất bại" } };
            }
        }

        [CheckLogin]
        public ActionResult ListWorkByResource(WorkModel model)
        {
            model.CurrentPage = model.CurrentPage == 0 ? 1 : model.CurrentPage;
            //WorkModel model = new WorkModel();

            var userid = AuthenUser.CurrentUser().UserID;
            var resourceID = AuthenUser.CurrentUser().ResourceID;

            var lstwork = db.tblWorks.Where(m => m.UserID == userid && m.ResourceID == resourceID).ToList().OrderByDescending(m => m.Createdate).ToList();
            foreach (var item in lstwork)
            {
                WorkItemModel workmodel = new WorkItemModel();
                workmodel.WorkId = item.WorkId;
                workmodel.Description = item.Description;
                workmodel.WorkName = item.WorkName;
                workmodel.MediaUrl = item.MediaUrl;
                workmodel.Createdate = item.Createdate;
                workmodel.Status = (int)item.Status;
                workmodel.NoteByUser = item.NoteByUser;
                workmodel.ResourceName = db.tblResources.FirstOrDefault(n => n.ResourceID == item.ResourceID).Description;
                workmodel.FromUser = db.tblUsers.FirstOrDefault(n => n.UserID == item.UserID).Fullname;

                //Url = "/DATA" + resource.FirstOrDefault(k => k.ResourceID == m.ResourceID).ResourceUrl + "/" + m.MediaUrl
                model.ListItem.Add(workmodel);
            }
            ViewBag.ResourceName = AuthenUser.CurrentUser().ResourceName;

            var total = model.ListItem.Count();
            ViewBag.CurrentPage = model.CurrentPage;
            ViewBag.TotalPage = (int)Math.Ceiling((decimal)total / 10);

            model.ListItem = model.ListItem.Skip((model.CurrentPage - 1) * 10).Take(10).ToList();

            return View(model);
        }
        [CheckLogin]
        [HttpPost]
        public JsonResult GetWork(int workId)
        {
            WorkItemModel obj = new WorkItemModel();
            IQueryable<tblRole> role = db.tblRoles;
            var resourceID = AuthenUser.CurrentUser().ResourceID;
            var objwork = db.tblWorks.FirstOrDefault(m => m.WorkId == workId);

            obj.WorkId = objwork.WorkId;
            obj.WorkName = objwork.WorkName;
            obj.Description = objwork.Description;
            obj.NoteByLeader = objwork.NoteByLeader;
            obj.NoteByUser = objwork.NoteByUser;
            obj.Status = (int)objwork.Status;

            var lstWorkUser = db.tblWorkUsers.Where(n => n.WorkId == workId).ToList();
            //lst user theo pb
            var lstUser = db.tblUsers.Where(m => m.ResourceID == resourceID).ToList();

            List<tbl_User> lstUserNew = new List<tbl_User>();
            foreach (var item in lstUser)
            {
                if (lstWorkUser.Any(p => p.ToUser == item.UserID))
                {
                    var i = new tbl_User()
                    {
                        UserID = item.UserID,
                        Fullname = item.Fullname,
                        ResourceName = db.tblResources.FirstOrDefault(m => m.ResourceID == item.ResourceID).Description,
                        CheckStatus = 1
                    };
                    lstUserNew.Add(i);
                }
                else
                {
                    var i = new tbl_User()
                    {
                        UserID = item.UserID,
                        Fullname = item.Fullname,
                        CheckStatus = 0
                    };
                    lstUserNew.Add(i);
                }
            }
            obj.lstUserWork = lstUserNew;
            db.Dispose();
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [CheckLogin]
        public ActionResult AddWorkToUser(int workId, string toUser, string notebyleader)
        {
            try
            {
                char[] delimiters = { ' ', ',' };
                string[] petsArray = toUser.Split(delimiters);
                //xóa cac user được assigned 
                var workobj = db.tblWorkUsers.Where(m => m.WorkId == workId);
                if (workobj.Count() > 0)
                {
                    db.tblWorkUsers.RemoveRange(workobj);
                    db.SaveChanges();
                }
                //add user được assigned
                foreach (var user in petsArray)
                {
                    tblWorkUser workuser = new tblWorkUser();
                    workuser.WorkId = workId;
                    workuser.ToUser = Int32.Parse(user);
                    workuser.Status = 1;

                    db.tblWorkUsers.Add(workuser);
                    var email = db.tblUsers.FirstOrDefault(k => k.UserID == workuser.ToUser).Email;
                    var workname = db.tblWorks.FirstOrDefault(n => n.WorkId == workId).WorkName;
                    var Description = db.tblWorks.FirstOrDefault(l => l.WorkId == workId).Description;

                    var subject = "Đầu việc được giao :" + workname;
                    var msg = "Mô tả : " + Description +
                              "ghi chú " + notebyleader;
                    
                    SendMailTima.SendEmail(email, subject, msg);
                }
                //update status tblwork
                tblWork wk = new tblWork();
                wk = db.tblWorks.Find(workId);
                db.Entry(wk).State = EntityState.Modified;
                wk.NoteByLeader = notebyleader;
                wk.AssignByUser = AuthenUser.CurrentUser().UserID;
                wk.Status = StatusEnum.DangXuLy.GetHashCode();

                db.SaveChanges();
                db.Dispose();
                return new JsonResult { Data = new { Status = 1, Message = "Giao việc thành công" } };
            }
            catch (Exception e)
            {
                ////var a = e.Message;
                return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! giao việc  thất bại" } };
            }
        }
        [HttpPost]
        public JsonResult GetListUserWork(string userID, int workid)
        {
            char[] delimiters = { ' ', ',' };
            string[] petsArray = userID.Split(delimiters);
            WorkItemModel objuser = new WorkItemModel();


            var resourceID = AuthenUser.CurrentUser().ResourceID;
            var lstUser = db.tblUsers.Where(m => m.ResourceID == resourceID).ToList();

            if (userID != "")
            {
                //lấy lst còn lại trong workuser 
                List<tbl_User> lstUserWork = new List<tbl_User>();
                foreach (var userid in petsArray)
                {
                    int id = Int32.Parse(userid);
                    var obj = db.tblUsers.FirstOrDefault(m => m.UserID == id);
                    var i = new tbl_User()
                    {
                        UserID = obj.UserID,
                        Fullname = obj.Fullname
                    };
                    lstUserWork.Add(i);
                }
                List<tbl_User> lstUserNew = new List<tbl_User>();
                foreach (var item in lstUser)
                {
                    //các user # trong tblworkuser theo workid
                    if (!lstUserWork.Any(p => p.UserID == item.UserID))
                    {
                        var k = new tbl_User()
                        {
                            UserID = item.UserID,
                            Fullname = item.Fullname
                        };
                        lstUserNew.Add(k);
                    }
                }
                objuser.lstUserWork = lstUserNew;
            }
            else
            {
                //xóa hết ds user được assigned cũ lấy add ds mới
                List<tbl_User> lstUserNew = new List<tbl_User>();
                foreach (var item in lstUser)
                {
                    var k = new tbl_User()
                    {
                        UserID = item.UserID,
                        Fullname = item.Fullname
                    };
                    lstUserNew.Add(k);
                }
                objuser.lstUserWork = lstUserNew;
            }
            return Json(objuser, JsonRequestBehavior.AllowGet);
        }

        [CheckLogin]
        public ActionResult DeleteWork(int workID)
        {
            var objwork = db.tblWorks.FirstOrDefault(m => m.WorkId == workID);
            if (objwork != null)
            {
                if (objwork.Status == StatusEnum.DangXuLy.GetHashCode())
                {
                    return new JsonResult { Data = new { Status = 0, Message = "Đầu việc đang xử lý , không thể xóa" } };
                }
                if (objwork.Status == StatusEnum.HoanThanh.GetHashCode())
                {
                    return new JsonResult { Data = new { Status = 0, Message = "Đầu việc đã hoàn thành , không thể xóa" } };
                }
                tblWork work = db.tblWorks.Find(workID);
                db.tblWorks.Remove(work);
                db.SaveChanges();
                db.Dispose();

                return new JsonResult { Data = new { Status = 1, Message = "Xóa đầu việc thành công" } };

            }
            else
            {
                return new JsonResult { Data = new { Status = 0, Message = "Xóa đầu việc thất bại" } };
            }

        }
        [CheckLogin]
        [HttpPost]
        public JsonResult GetWorkById(int workId)
        {
            WorkItemModel objwork = new WorkItemModel();
            var work = db.tblWorks.FirstOrDefault(m => m.WorkId == workId);
            if (work != null)
            {
                objwork.WorkId = work.WorkId;
                objwork.WorkName = work.WorkName;
                objwork.Description = work.Description;
                objwork.ResourceID = (int)work.ResourceID;
                objwork.MediaUrl = work.MediaUrl;
            }
            db.Dispose();
            return Json(objwork, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateWorkById(int workId, string workName, string description, int resourceID)
        {
            string fname = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];

                        var supportedTypes = new[] { "txt", "doc", "docx", "pdf", "xls", "xlsx" };
                        var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                        //Checking for Internet Explorer
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string date = DateTime.Now.ToString("dd/MM/yyyy HH:mm").Replace("/", "_").Replace(" ", "_").Replace(":", "_");

                            fname = Path.GetFileName(date + file.FileName);
                            if (fileExt.Contains("exe"))
                            {
                                return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Upload file thất bại" } };
                            }
                        }
                        IQueryable<tblResource> resource = db.tblResources;
                        var resourceUrl = resource.FirstOrDefault(k => k.ResourceID == resourceID).ResourceUrl;
                        var path = "~/Data" + resourceUrl;
                        // Get the complete folder path and store the file inside it.  
                        var newpath = Path.Combine(Server.MapPath(path), fname);
                        file.SaveAs(newpath);
                    }
                    // Returns message that successfully uploaded  

                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            if (workId > 0)
            {
                var Objwork = db.tblWorks.FirstOrDefault(m => m.WorkId == workId);
                if (Objwork != null)
                {
                    tblWork obj = new tblWork();
                    obj = db.tblWorks.Find(Objwork.WorkId);
                    db.Entry(obj).State = EntityState.Modified;
                    obj.WorkName = workName;
                    if (fname != "")
                    {
                        RemoveFile(workId, resourceID);
                        obj.MediaUrl = fname;
                    }
                    obj.Description = description;
                    obj.ResourceID = resourceID;
                    db.SaveChanges();
                    db.Dispose();
                    return new JsonResult { Data = new { Status = 1, Message = "Cập nhật việc thành công" } };
                }
                return new JsonResult { Data = new { Status = 0, Message = "Lỗi.Không tồn tại đầu việc" } };
            }
            return new JsonResult { Data = new { Status = 0, Message = "Lỗi.Không tồn tại đầu việc" } };
        }
        public void RemoveFile(int workId, int resourceID)
        {
            IQueryable<tblResource> resource = db.tblResources;
            var resourceUrl = resource.FirstOrDefault(k => k.ResourceID == resourceID).ResourceUrl;
            var path = "~/Data" + resourceUrl;
            var obj = db.tblWorks.FirstOrDefault(m => m.WorkId == workId);
            if (!string.IsNullOrEmpty(obj.MediaUrl))
            {
                //string fullPath = Request.MapPath(path + obj.MediaUrl);
                var fullPath = Path.Combine(Server.MapPath(path), obj.MediaUrl);
                //if (System.IO.File.Exists(fullPath))
                //{
                System.IO.File.Delete(fullPath);
                //}
            }
        }
    }
}