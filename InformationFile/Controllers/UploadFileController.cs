﻿
using InformationFile.Extention;
using InformationFile.Models.UploadFile;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Controllers
{
    public class UploadFileController : Controller
    {
        private TimaFMEntities db = new TimaFMEntities();
        // GET: UploadFile
        [CheckLogin]
        public ActionResult Index(UploadFileModel model)
        {
            //UploadFileModel model = new UploadFileModel();
            model.CurrentPage = model.CurrentPage == 0 ? 1 : model.CurrentPage;
            IQueryable<tblMedia> media = db.tblMedias;
            IQueryable<tblUser> user = db.tblUsers;
            IQueryable<tblResource> resource = db.tblResources;
            IQueryable<tblRole> role = db.tblRoles;
            var userid = AuthenUser.CurrentUser().UserID;
            var tblrole = role.FirstOrDefault(k => k.UserID == userid && k.ResourceID == model.ResourceID);
            if (tblrole != null)
            {
                if (tblrole.CmdInsert == true)
                {
                    model.CmdInsert = 1;
                    ViewBag.CmdInsert = 1;
                }
                if (tblrole.CmdDelete == true)
                {
                    model.CmdDelete = 1;
                    ViewBag.CmdDelete = 1;
                }
                if (tblrole.CmdEdit == true)
                {
                    //model.CmdEdit = 1;
                    ViewBag.CmdEdit = 1;
                }
            }
            

            ViewBag.DepamentName = db.tblResources.FirstOrDefault(r => r.ResourceID == model.ResourceID).Description;

            var lstmedia = media.Where(m => m.ResourceID == model.ResourceID).ToList();
            if (model.Str != null && model.Str.Trim() != "")
            {
                lstmedia = lstmedia.Where(n => n.MediaName == model.Str.Trim()).ToList();
            }
            if (model.EffectiveStatusSearch >0 )
            {
                lstmedia = lstmedia.Where(n => n.EffectiveStatus == model.EffectiveStatusSearch).ToList();
            }
            var total = media.Count(m => m.ResourceID == model.ResourceID);
            model.ListItem = lstmedia.Select(m => new UploadFileModel()
            {
                UserID = m.UserID,
                MediaID = m.MediaID,
                MediaName = m.MediaName,
                MediaUrl = m.MediaUrl,
                MediaType = m.MediaType,
                Createdate = m.Createdate,
                DeparmentName = resource.FirstOrDefault(n => n.ResourceID == m.ResourceID) == null ? "" : resource.FirstOrDefault(n => n.ResourceID == m.ResourceID).Description,
                FullName = user.FirstOrDefault(n => n.UserID == m.UserID) == null ? "" : user.FirstOrDefault(n => n.UserID == m.UserID).Fullname,
                Url = "/DATA" + resource.FirstOrDefault(k => k.ResourceID == m.ResourceID).ResourceUrl + "/" + m.MediaUrl,
                MediaCode = m.MediaCode,
                EffectiveDateView = m.EffectiveDate,
                EffectiveStatus = m.EffectiveStatus

            }).ToList().Skip((model.CurrentPage - 1) * 10).Take(10).ToList();

            model.ResourceID = model.ResourceID;
            db.Dispose();
            var effectiveStatus = EnumHelper.GetEnumToList(typeof(EffectiveStatusEnum));
            if (effectiveStatus != null && effectiveStatus.Any())
            {
                model.ListeffectiveStatus = effectiveStatus;
                SelectListItem itemFirst = new SelectListItem()
                {
                    Text = "-- Tất cả --",
                    Value = "0"
                };
                model.ListeffectiveStatus.Insert(0, itemFirst);
            }
            

            ViewBag.CurrentPage = model.CurrentPage;
            ViewBag.TotalPage = (int)Math.Ceiling((decimal)total / 10);
            return View(model);
        }
        [CheckLogin]
        [HttpPost]
        public ActionResult UploadFiles(string mediaCode, string effectiveDate,int effectiveStatus, string mediaName, int ResourceID)
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;
                        var supportedTypes = new[] { "txt", "doc", "docx", "pdf", "xls", "xlsx" };


                        var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                        //Checking for Internet Explorer
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string date = DateTime.Now.ToString("dd/MM/yyyy HH:mm").Replace("/","_").Replace(" ", "_").Replace(":","_");
                          
                             fname = Path.GetFileName(date + file.FileName);
                            //fname = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                            if (fileExt.Contains("exe"))
                            {
                                return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Upload file thất bại" } };
                            }
                        }
                        IQueryable<tblResource> resource = db.tblResources;
                        var resourceUrl = resource.FirstOrDefault(k => k.ResourceID == ResourceID).ResourceUrl;
                        var path = "~/Data" + resourceUrl;

                        // Get the complete folder path and store the file inside it.  
                        var newpath = Path.Combine(Server.MapPath(path), fname);

                        file.SaveAs(newpath);
                        IQueryable<tblUser> user = db.tblUsers;
                        var userid = AuthenUser.CurrentUser().UserID;
                        var tblUser = user.FirstOrDefault(m => m.UserID == userid);

                        tblMedia tblmedia = new tblMedia();
                        tblmedia.MediaCode = mediaCode;
                        tblmedia.EffectiveStatus = effectiveStatus;
                        tblmedia.EffectiveDate = DateTimeUtility.ConvertStringToDate(effectiveDate.Trim());
                        tblmedia.MediaName = mediaName;
                        tblmedia.UserID = userid;
                        tblmedia.MediaUrl = fname;
                        tblmedia.Createdate = DateTime.Now;
                        tblmedia.MediaType = fileExt;
                        tblmedia.ResourceID = ResourceID;
                        db.tblMedias.Add(tblmedia);
                        db.SaveChanges();
                        db.Dispose();
                        int id = tblmedia.MediaID;
                        if (id > 0)
                        {
                            return new JsonResult { Data = new { Status = 1, Message = "Upload file thành công .!" } };
                        }
                        else
                        {
                            return new JsonResult { Data = new { Status = 1, Message = "Lỗi .! Upload file thất bại" } };
                        }

                    }
                    // Returns message that successfully uploaded  
                    return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Upload file thất bại" } };
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Upload file thất bại" } };
            }
        }
        [CheckLogin]
        [HttpPost]
        public JsonResult ExporFile(int id)
        {
            string fileName = "";
            if (id > 0)
            {
                IQueryable<tblMedia> media = db.tblMedias;
                fileName = media.FirstOrDefault(m => m.MediaID == id).MediaUrl;

                return Json(new { fileName = fileName, errorMessage = "" });
            }
            else
            {
                return Json(new { fileName = fileName, errorMessage = "Không tìm thấy file" });
            }

        }
        [CheckLogin]
        [HttpGet]
        public ActionResult Download(string file, int resourceID)
        {
            //get the temp folder and file path in server
            IQueryable<tblResource> resource = db.tblResources;
            var resourceUrl = resource.FirstOrDefault(k => k.ResourceID == resourceID).ResourceUrl;
            var path = "~/Data" + resourceUrl;

            string fullPath = Path.Combine(Server.MapPath(path), file);

            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(fullPath, "application/vnd.ms-excel", file);
        }

        [CheckLogin]
        public ActionResult DeleteFile(int id)
        {
            var media = db.tblMedias.FirstOrDefault(m => m.MediaID == id);
            if (media != null)
            {
                tblMedia mdeia = db.tblMedias.Find(id);
                db.tblMedias.Remove(mdeia);
                db.SaveChanges();
                db.Dispose();

                return new JsonResult { Data = new { Status = 1, Message = "Xóa file thành công" } };

            }
            else
            {
                return new JsonResult { Data = new { Status = 0, Message = "Xóa file thất bại" } };
            }

        }

        [CheckLogin]
        [HttpPost]
        public JsonResult GetMediaById(int mediaID)
        {
            UploadFileModel media = new UploadFileModel();
            var obj = db.tblMedias.FirstOrDefault(m=>m.MediaID ==mediaID);
            if (obj != null)
            {
                media.MediaID = obj.MediaID;
                media.MediaName = obj.MediaName;
                media.MediaCode = obj.MediaCode;
                media.ResourceID = (int)obj.ResourceID;
                if (obj.EffectiveDate != null)
                {
                    media.EffectiveDateEdit = (DateTime)obj.EffectiveDate;
                }
                media.EffectiveStatus = obj.EffectiveStatus;
                media.MediaUrl = obj.MediaUrl;
            }
            db.Dispose();
            return Json(media, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult UpdateMediaById(int mediaID, string mediaCode, string mediaName, string effectiveDate, int effectiveStatus)
        {
            string fname = "";
            string fileExtnew = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];

                        var supportedTypes = new[] { "txt", "doc", "docx", "pdf", "xls", "xlsx" };
                        var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                        //Checking for Internet Explorer
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string date = DateTime.Now.ToString("dd/MM/yyyy HH:mm").Replace("/", "_").Replace(" ", "_").Replace(":", "_");

                            fname = Path.GetFileName(date + file.FileName);
                            if (fileExt.Contains("exe"))
                            {
                                return new JsonResult { Data = new { Status = 0, Message = "Lỗi .! Upload file thất bại" } };
                            }
                        }
                        fileExtnew = fileExt;
                        IQueryable<tblResource> resource = db.tblResources;
                        var resourceID = db.tblMedias.FirstOrDefault(k => k.MediaID == mediaID).ResourceID;
                        var resourceUrl = resource.FirstOrDefault(k => k.ResourceID == resourceID).ResourceUrl;
                        var path = "~/Data" + resourceUrl;
                        // Get the complete folder path and store the file inside it.  
                        var newpath = Path.Combine(Server.MapPath(path), fname);
                        file.SaveAs(newpath);
                    }
                    // Returns message that successfully uploaded  

                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            if (mediaID > 0)
            {
                var objmedia = db.tblMedias.FirstOrDefault(m => m.MediaID== mediaID);
                if (objmedia != null)
                {
                    tblMedia obj = new tblMedia();
                    obj = db.tblMedias.Find(objmedia.MediaID);
                    db.Entry(obj).State = EntityState.Modified;
                    obj.MediaCode = mediaCode;
                    obj.MediaName = mediaName;
                    obj.EffectiveDate = DateTimeUtility.ConvertStringToDate(effectiveDate.Trim());
                    obj.EffectiveStatus = effectiveStatus;
                    if (fname != "")
                    {
                        //RemoveFile(workId, resourceID);
                        obj.MediaUrl = fname;
                        obj.MediaType = fileExtnew;
                    }
                    db.SaveChanges();
                    db.Dispose();
                    return new JsonResult { Data = new { Status = 1, Message = "Cập nhật tài liệu thành công" } };
                }
                return new JsonResult { Data = new { Status = 0, Message = "Lỗi.Không tồn tại tài liệu" } };
            }
            return new JsonResult { Data = new { Status = 0, Message = "Lỗi.Không tồn tại tài liệu" } };
        }

        [CheckLogin]
        public ActionResult ViewFifle(int MediaID)
        {
            IQueryable<tblMedia> media = db.tblMedias;
            IQueryable<tblUser> user = db.tblUsers;
            IQueryable<tblResource> resource = db.tblResources;
            string url = string.Empty;
            var objmedia = media.FirstOrDefault(m => m.MediaID == MediaID);
            if (objmedia != null)
            {
                url = "/DATA" + resource.FirstOrDefault(k => k.ResourceID == objmedia.ResourceID).ResourceUrl + "/" + objmedia.MediaUrl;
            }
            ViewBag.Url = url;
            return View("ViewFifle");
        }

    }
}