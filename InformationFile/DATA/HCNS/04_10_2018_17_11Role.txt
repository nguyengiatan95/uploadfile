using InformationFile.Extention;
using InformationFile.Models.Role;
using InformationFile.Models.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Controllers
{
    public class RoleController  Controller
    {
        private TimaFMEntities db = new TimaFMEntities();
         GET Role
        [CheckLogin]
        public ActionResult Index(RoleModel model)
        {

            RoleModel model = new RoleModel();
            IQueryabletblResource resource = db.tblResources;
            var lstResource = db.tblResources.ToList();
            lstResource.Add(new tblResource() { ResourceID = 0, Description = --T?t c?-- });
            model.ResourceID = 1;
            ViewBag.UserName = AuthenUser.CurrentUser().Fullname;
            var lstUser = db.tblUsers.ToList();
            if (model.ResourceID == 0)
            {

                Listtbl_User lstuser = new Listtbl_User();

                foreach (var item in lstUser)
                {
                    tbl_User user = new tbl_User();
                    user.UserID = item.UserID;
                    user.Fullname = item.Fullname;
                    user.Email = item.Email;
                    user.ResourceID = 0;
                    lstuser.Add(user);
                }
                model.ListItem = lstuser;

            }
            else
            {


                var query = (from rl in db.tblRoles
                             join us in db.tblUsers on rl.UserID equals us.UserID
                             where rl.ResourceID == model.ResourceID
                             select new { ResourceID = rl.ResourceID, UserID = us.UserID, Fullname = us.Fullname, Email = us.Email, CmdView = rl.CmdView, CmdInsert = rl.CmdInsert, CmdDelete = rl.CmdDelete }).ToList();

                Listtbl_User lstuser = new Listtbl_User();

                foreach (var qr in query)
                {
                    foreach (var item in lstUser)
                    {
                        if (qr.UserID == item.UserID)
                        {
                            var ls = lstuser.FirstOrDefault(n = n.UserID == item.UserID);
                            x�a item lstuser v?a add - else 
                            var st = lstuser.Find(c = c.UserID == item.UserID);
                            lstuser.Remove(st);

                            tbl_User user = new tbl_User();
                            user.UserID = qr.UserID;
                            user.Fullname = qr.Fullname;
                            user.Email = qr.Email;
                            user.ResourceID = qr.ResourceID;
                            user.CmdDelete = qr.CmdDelete;
                            user.CmdInsert = qr.CmdInsert;
                            user.CmdView = qr.CmdView;
                            lstuser.Add(user);

                        }
                        else
                        {
                            var ls = lstuser.FirstOrDefault(n = n.UserID == item.UserID);
                            if (ls == null)
                            {
                                tbl_User user = new tbl_User();
                                user.UserID = item.UserID;
                                user.Fullname = item.Fullname;
                                user.ResourceID = qr.ResourceID;
                                user.Email = item.Email;
                                lstuser.Add(user);
                            }

                        }
                    }
                }
                model.ListItem = lstuser;
                end else
            }
            var a = 2;
            var count = model.ListItem.Count();
            model.ListItem  =  model.ListItem.Skip((model.CurrentPage - 1)  10).Take(10).ToList();
        
            model.ListResource = lstResource;
            db.Dispose();
            ViewBag.CurrentPage = model.CurrentPage ;
            ViewBag.TotalPage = (int)Math.Ceiling((decimal)count  10);
            return View(model);
        }
        [HttpPost]
        public JsonResult GetResource(int resourceID)
        {
            RoleItemModel obj = new RoleItemModel();
            IQueryabletblRole role = db.tblRoles;
            var userid = AuthenUser.CurrentUser().UserID;
            var objRole = role.FirstOrDefault(m = m.ResourceID == resourceID);
            obj.ResourceName = db.tblResources.FirstOrDefault(n = n.ResourceID == resourceID).Description;
            obj.RoleID = objRole.RoleID;
            obj.ResourceID = objRole.ResourceID;
            obj.CmdInsert = objRole.CmdInsert;
            obj.CmdView = objRole.CmdView;
            obj.CmdDelete = objRole.CmdDelete;

            obj.UserID = objRole.UserID;
            db.Dispose();

            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [CheckLogin]
        public ActionResult UpdateRole(int userid, int resourceid, bool role, int type)
        {

            IQueryabletblRole tblrole = db.tblRoles;
            var ObjRole = tblrole.FirstOrDefault(m = m.ResourceID == resourceid && m.UserID == userid);
            tblRole obj = new tblRole();

            if (ObjRole != null)
            {
                update
                 type=1 = view , type=2=insert ,type =3=delete
                if (type == 1)
                {
                    obj = db.tblRoles.Find(ObjRole.RoleID);
                    db.Entry(obj).State = EntityState.Modified;
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdView = role;
                    db.SaveChanges();
                }
                if (type == 2)
                {
                    obj = db.tblRoles.Find(ObjRole.RoleID);
                    db.Entry(obj).State = EntityState.Modified;
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdInsert = role;
                    db.SaveChanges();
                }
                if (type == 3)
                {
                    obj = db.tblRoles.Find(ObjRole.RoleID);
                    db.Entry(obj).State = EntityState.Modified;
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdDelete = role;
                    db.SaveChanges();
                }
                int id = obj.RoleID;
                db.Dispose();
                if (id  0)
                {
                    return new JsonResult { Data = new { Status = 1, Message = Update th�nh c�ng .! } };
                }
                else
                {
                    return new JsonResult { Data = new { Status = 1, Message = L?i .! Update th?t b?i } };
                }
            }
            else
            {
                insert
                if (type == 1)
                {
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdView = role;
                    db.tblRoles.Add(obj);
                    db.SaveChanges();
                }
                if (type == 2)
                {
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdInsert = role;
                    db.tblRoles.Add(obj);
                    db.SaveChanges();
                }
                if (type == 3)
                {
                    obj.UserID = userid;
                    obj.ResourceID = resourceid;
                    obj.CmdDelete = role;
                    db.tblRoles.Add(obj);
                    db.SaveChanges();
                }
                int id = obj.RoleID;
                db.Dispose();
                if (id  0)
                {
                    return new JsonResult { Data = new { Status = 1, Message = Update th�nh c�ng .! } };
                }
                else
                {
                    return new JsonResult { Data = new { Status = 1, Message = L?i .! Update th?t b?i } };
                }

            }

            return new JsonResult { Data = new { Status = 0, Message =  } };
        }
        [CheckLogin]
        public ActionResult GetRoleUser(int userID, int resourceId)
        {
            IQueryabletblRole role = db.tblRoles;
            var obj = role.FirstOrDefault(m = m.UserID == userID && m.ResourceID == resourceId);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

    }
}