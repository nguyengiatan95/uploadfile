﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InformationFile.Extention
{
    public static class EnumHelper
    {
        public static List<SelectListItem> GetEnumToList(Type type)
        {
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            foreach (object item in Enum.GetValues(type))
            {
                SelectListItem objSelectListItem = new SelectListItem();
                objSelectListItem.Value = ((int)item).ToString();
                objSelectListItem.Text = GetDescription((Enum)item);
                lstSelectListItem.Add(objSelectListItem);
            }
            lstSelectListItem = lstSelectListItem.OrderBy(m => m.Value).ToList();
            return lstSelectListItem;
        }

        public static string GetDescription(this Enum value)
        {
            try
            {
                var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
                return da.Length > 0 ? da[0].Description : value.ToString();
            }
            catch
            {
                return null;
            }
        }

    }
}