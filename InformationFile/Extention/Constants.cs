﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace InformationFile.Extention
{
    public enum StatusEnum
    {
            [Description("Đã Gửi")]
            DaGui = 1,
            [Description("Đang xử lý")]
            DangXuLy = 2,
            [Description("Hoàn thành")]
            HoanThanh = 3

    }
    public enum EffectiveStatusEnum
    {
        [Description("Hiệu lực toàn phần")]
        HieuLucToanPhan = 1,
        [Description("Hiệu lực từng phần")]
        HieuLucTungPhan = 2,
        [Description("Hết hiệu lực")]
        HetHieuLuc = 3
    }
    public enum TypeUserEnum
    {
        //0 -các nhân viên có mail @tima ko có database
        [Description("Nhân viên tima")]
        NhanVien = 0,
        [Description("Trưởng phòng")]
        TruongPhong = 1,
        [Description("BOD")]
        BOD = 2,
        [Description("Toàn quyền doc.tima.vn")]
        PhanQuyen = 3
    }
    public enum TypeResource
    {
        //0 -các nhân viên có mail @tima ko có database
        [Description("Phòng xem chung")]
        TypeAll = 1
    }
}