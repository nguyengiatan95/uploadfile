﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace InformationFile.Extention
{
    public class DateTimeUtility
    {
        public static string DATE_FORMAT = "dd/MM/yyyy";
        public static string DATE_FORMAT_VN2 = "dd/MM/yyyy";
        public static string DATE_FORMAT_VN = "dd/MM/yyyy";
        public static string DATE_FORMAT_VN3 = "MM/dd/yyyy";
        public static DateTime ConvertStringToDate(string strDate)
        {
            DateTime result;
            try
            {
                IFormatProvider celture = new CultureInfo("fr-FR", true);

                var arrayDate = strDate.Split('/');
                if (arrayDate[0].Length < 2)
                {
                    arrayDate[0] = "0" + arrayDate[0];
                }
                if (arrayDate[1].Length < 2)
                {
                    arrayDate[1] = "0" + arrayDate[1];
                }
                strDate = arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2];
                result = DateTime.ParseExact(strDate, DATE_FORMAT, celture, DateTimeStyles.NoCurrentDateDefault);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}