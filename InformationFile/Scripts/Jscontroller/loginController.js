﻿
var loginController = (function (windows, undefined) {

    //function validateLogin() {
    //    $(document).on('click', '#loginform', function () {
    //        debugger
    //        $.ajax({
    //            url: $(this).attr("action"),
    //            type: "POST",
    //            data: "",
    //            dataType: "json",
    //            success: function (Data) {
    //                debugger
    //                console(Data);
    //            }
    //        })
    //    })
    //}

    function validuserlogin() {
        $('#loginform').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'icon icon-thumbs-up',
                invalid: 'fontawesome-hand-left',
                validating: 'icon icon-thumbs-up'
            }
        })
            .on('success.form.fv', function (e) {
                debugger
                // Prevent form submission
                // loading
                //loadingapp.show();
                //e.preventDefault();
                var options = {
                    url: $(this).attr("action"),
                    success: function (data, status) {
                        debugger
                        console.log(data);
                        if (data.Status == 1) {
                            // redirect to dashboard
                            window.location.href = "/Home/Index";
                        }
                        else {
                            pnotifyapp.error(data.Message);
                        }

                        loadingapp.hide();
                    }
                };
                $(this).ajaxSubmit(options);
            });
    }

    return {
        validuserlogin: validuserlogin
    }

})(window);